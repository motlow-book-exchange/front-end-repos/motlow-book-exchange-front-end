# testing123

> My best Nuxt.js project

## Build Setup

``` bash
# install dependencies
$ npm run install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).


NOTE: Icons have been changed to googles material design icons @ material.io
DEFAULT ICONS: https://materialdesignicons.com/
CURRENT ICONS: https://material.io/resources/icons/?style=baseline

TODO: Remove dotprop library if not needed.  check default.vue file
TODO: Update this readme


Remember npm run generate is what is needed when production is being pushed in netlify.

https://console.developers.google.com/apis/credentials?project=motlowbookexchan-1561074725199  -  Update credentials to this