// Testing Auth Problems
export default function({ app }) {
  app.$auth.onError((error, name, endpoint) => {
    console.error(name, error);
  });

  app.$auth.onRedirect((to, from) => {
    console.log('test');
    console.error(to);
    // you can optionally change `to` by returning a new value
  });
}